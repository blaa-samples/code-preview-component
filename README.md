# code-preview-component

React component for displaying code with examples in multiple languages.

Demo of the WIP-State can be seen here: [External Test - Gitlab Pages](https://blaa-samples.gitlab.io/test-code-preview/), [Internal Demo - Gitlab Pages](https://blaa-samples.gitlab.io/code-preview-component/).

## To Dos

### v1.0

- [x] Switch Language
- [x] Highlight Code
  - [ ] Props: Enable/Disable Line Numbers
  - [ ] Props: Change Style
- [ ] Compile UML to graphic

### Ideas

- Side-by-side viewing code. As example: 
  - HTML, JS and CSS to get the whole picture
  - Compare alike snippets  
- Show Result of code - Dartpad, Codepen integration

## Example

Currently it's possible to add a Language and a Code Snippet.

```jsx
import CodePreview from "code-preview-component";

const langMap = new Map<Langs, string>();
// Init the Langauges
langMap.set(Langs.JavaScript, `console.log("Hello world!")`);

return (
  <CodePreview languageEntries={this.state.languageEntries} />
);
```

## Note

The [react-syntax-highlighter](https://www.npmjs.com/package/react-syntax-highlighter) is lazy loaded through `React.lazy` for faster feedback. For this the async implementation of the highlighter is used. This also splits the highlighter-dependency into chunks while building, which are dynamically loaded, depending on language currently displayed.
There are also custom type-definitions provided for react-syntax-highlighter, as the ones on [@types/react-syntax-highlighter](https://www.npmjs.com/package/@types/react-syntax-highlighter) don't yet include the async lazy loading version.

[@material-ui/core](https://www.npmjs.com/package/@material-ui/core) and [@material-ui/icons](https://www.npmjs.com/package/@material-ui/icons) is just used as webpack.external, so the using module can handle code splitting.

## Icon - Sources

In General icons are taken from [DevIcon](http://konpa.github.io/devicon/).

- Rustlang Logo: [Rustlang Icon Logo](https://www.rust-lang.org/logos/rust-logo-blk.svg)
- For this logo the size and viewbox has been adjustet, as the original contains large borders: [Bash Logo](https://bashlogo.com/img/symbol/svg/full_colored_dark.svg)