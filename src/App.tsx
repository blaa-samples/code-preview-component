import * as React from "react";
import { Langs, CodePreview } from './lib';

export interface AppState {
  languageEntries: Map<Langs, string>
}

class App extends React.Component<any, AppState> {
  constructor(props: any) {
    super(props);

    const langMap = new Map<Langs, string>();

    // Init the Langauges
    langMap.set(Langs.JavaScript, `function createStyleObject(classNames, style) {
        return classNames.reduce((styleObject, className) => {
          return {...styleObject, ...style[className]};
        }, {});
      }
      function createClassNameString(classNames) {
        return classNames.join(' ');
      }
      function createChildren(style, useInlineStyles) {
        let childrenCount = 0;
        return children => {
          childrenCount += 1;
          return children.map((child, i) => createElement({
            node: child,
            style,
            useInlineStyles,
            key:\`code-segment-$\{childrenCount}-$\{i}\`
          }));
        }
      }
      function createElement({ node, style, useInlineStyles, key }) {
        const { properties, type, tagName, value } = node;
        if (type === "text") {
          return value;
        } else if (tagName) {
          const TagName = tagName;
          const childrenCreator = createChildren(style, useInlineStyles);
          const props = (
            useInlineStyles
            ?
            { style: createStyleObject(properties.className, style) }
            :
            { className: createClassNameString(properties.className) }
          );
          const children = childrenCreator(node.children);
          return <TagName key={key} {...props}>{children}</TagName>;
        }
      }`);
    langMap.set(Langs.UML, "UML Example");
    langMap.set(Langs.Cpp, `#include <iostream>
      using namespace std;
      
      int main() 
      {
          cout << "Hello, World!";
          return 0;
      }`);
    langMap.set(Langs.Gherkin, `Feature: Guess the word
  
      # The first example has two steps
      Scenario: Maker starts a game
        When the Maker starts a game
        Then the Maker waits for a Breaker to join
      
      # The second example has three steps
      Scenario: Breaker joins a game
        Given the Maker has started a game with the word "silky"
        When the Breaker joins the Maker's game
        Then the Breaker must guess a word with 5 characters`);
    langMap.set(Langs.Dart, `void main() {
        print('Hello, World!');
    }`);
    langMap.set(Langs.Go, `package main
  
      import "fmt"
      
      func main() {
          fmt.Println("hello world")
      }`);
    langMap.set(Langs.Less, `@hello: #dd4433;
      @world: #66bb33;
      
      .hello {
        color: @hello;
      }
      
      .world {
        color: @world;
      }`);
    langMap.set(Langs.PHP, `<?php phpinfo(); ?>`);
    langMap.set(Langs.TypeScript, `interface Person {
        firstName: string;
        lastName: string;
    }
    
    function greeter(person: Person) {
        return "Hello, " + person.firstName + " " + person.lastName;
    }
    
    let user = { firstName: "Jane", lastName: "User" };
    
    document.body.innerHTML = greeter(user);
    `);
    langMap.set(Langs.Java, `public class HelloWorld {
        public static void main(String[] args) {
            System.out.println("Hello, World!");
        }
    }`);
    langMap.set(Langs.Dockerfile, `FROM ubuntu:15.04
      COPY . /app
      RUN make /app
      CMD python /app/app.py`);
    langMap.set(Langs.CSharp, `// A Hello World! program in C#.
      using System;
      namespace HelloWorld
      {
          class Hello 
          {
              static void Main() 
              {
                  Console.WriteLine("Hello World!");
      
                  // Keep the console window open in debug mode.
                  Console.WriteLine("Press any key to exit.");
                  Console.ReadKey();
              }
          }
      }`);
    langMap.set(Langs.C, `int main() {
        printf("Hello, world!");
    }`);
    langMap.set(Langs.Sass, `$font-stack:    Helvetica, sans-serif;
      $primary-color: #333;
      
      body {
        font: 100% $font-stack;
        color: $primary-color;
      }`);
    langMap.set(Langs.Swift, `// Hello, World! Program
      import Swift
      print("Hello, World!")`);
    langMap.set(Langs.CSS3, `<style type="text/css">
      h1 {
        color: DeepSkyBlue;
      }
      </style>
      
      <h1>Hello, world!</h1>`);
    langMap.set(Langs.Rust, `fn main() {
        // The statements here will be executed when the compiled binary is called
    
        // Print text to the console
        println!("Hello World!");
    }`);
    langMap.set(Langs.Bash, `#!/bin/bash
      echo "hello world"`);
    this.state = {
      languageEntries: langMap
    }
  }

  render() {
    return (
      <CodePreview languageEntries={this.state.languageEntries} />
    );
  }
}

export default App;