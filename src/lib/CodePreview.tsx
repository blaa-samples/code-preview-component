import * as React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import LangsMenu from './preview/LangsMenu';
import { Langs } from './Languages';
import { CodeEntry } from './preview/CodeEntry';
import LanguageIcon from "./icons/LanguageIcon";

type CodePreviewProps = {
  languageEntries: Map<Langs, String>
}

export interface CodePreviewState {
  anchorEl: HTMLElement,
  language: Langs
}

export class CodePreview extends React.Component<CodePreviewProps, CodePreviewState> {
  state = {
    anchorEl: null as unknown as HTMLElement,
    language: this.props.languageEntries.entries().next().value[0],
  }

  constructor(props: CodePreviewProps) {
    super(props);
    this.handleShowLanguages = this.handleShowLanguages.bind(this);
  }

  handleShowLanguages = (event: React.MouseEvent) => {
    this.setState({
      anchorEl: event.currentTarget as HTMLElement
    })
  }

  handleChangeLanguage = (lang: Langs) => {
    this.setState({
      anchorEl: null as unknown as HTMLElement,
      language: lang
    })
  }

  render() {
    return (
      <div>
        <AppBar style={{ borderBottom: '1px solid #eeeeee' }} color="default" elevation={0} position="static">
          <Toolbar>
            <Tooltip title="Change Language">
              <Button onClick={this.handleShowLanguages}>
                <LanguageIcon language={this.state.language} />
              </Button>
            </Tooltip>
          </Toolbar>
        </AppBar>
        {
          <LangsMenu
            anchor={this.state.anchorEl}
            selected={this.state.language}
            open={this.state.anchorEl !== null}
            onClick={this.handleChangeLanguage}
            languages={Array.from(this.props.languageEntries.keys())}
          />
        }
        <CodeEntry language={this.state.language} content={this.props.languageEntries.get(this.state.language)} />
      </div>
    )
  }
}

export default CodePreview;