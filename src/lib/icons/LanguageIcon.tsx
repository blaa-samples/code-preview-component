import Tooltip from "@material-ui/core/Tooltip";
import * as React from "react";
import { Langs } from '../Languages';
import JavaScriptIcon from './single/JavaScriptIcon';
import TypeScriptIcon from './single/TypeScriptIcon';
import JavaIcon from './single/JavaIcon';
import GoIcon from './single/GoIcon';
import DockerIcon from './single/DockerIcon';
import CucumberIcon from './single/CucumberIcon';
import LessIcon from './single/LessIcon';
import PHPIcon from './single/PHPIcon';
import RubyIcon from './single/RubyIcon';
import SassIcon from './single/SassIcon';
import SwiftIcon from './single/SwiftIcon';
import CppIcon from './single/CppIcon';
import CSharpIcon from './single/CSharpIcon';
import CIcon from './single/CIcon';
import CSS3Icon from './single/CSS3Icon';
import DartIcon from './single/DartIcon';
import RustIcon from './single/RustIcon';
import BashIcon from './single/BashIcon';
import UMLIcon from './single/UMLIcon';

export interface LanguageIconProps {
  language: string,
  size?: number,
  disabled?: boolean,
  style?: React.CSSProperties
}

export default class LanguageIcon extends React.PureComponent<LanguageIconProps> {
  render() {
    let component;
    switch (this.props.language) {
      case Langs.JavaScript: {
        component = <JavaScriptIcon />;
        break;
      }
      case Langs.TypeScript: {
        component = <TypeScriptIcon />;
        break;
      }
      case Langs.Java: {
        component = <JavaIcon />;
        break;
      }
      case Langs.Go: {
        component = <GoIcon />;
        break;
      }
      case Langs.Dockerfile: {
        component = <DockerIcon />;
        break;
      }
      case Langs.Gherkin: {
        component = <CucumberIcon />;
        break;
      }
      case Langs.Less: {
        component = <LessIcon />;
        break;
      }
      case Langs.PHP: {
        component = <PHPIcon />;
        break;
      }
      case Langs.Ruby: {
        component = <RubyIcon />;
        break;
      }
      case Langs.Sass: {
        component = <SassIcon />;
        break;
      }
      case Langs.Swift: {
        component = <SwiftIcon />;
        break;
      }
      case Langs.Cpp: {
        component = <CppIcon />;
        break;
      }
      case Langs.CSharp: {
        component = <CSharpIcon />;
        break;
      }
      case Langs.C: {
        component = <CIcon />;
        break;
      }
      case Langs.CSS3: {
        component = <CSS3Icon />;
        break;
      }
      case Langs.Dart: {
        component = <DartIcon />;
        break;
      }
      case Langs.Rust: {
        component = <RustIcon />;
        break;
      }
      case Langs.Bash: {
        component = <BashIcon />;
        break;
      }
      case Langs.UML: {
        component = <UMLIcon />;
        break;
      }
      default: {
        const Error = React.lazy(() => import(/* webpackChunkName: "ErrorIcon" */"@material-ui/icons/Error"));
        component = <React.Suspense fallback={<span>x</span>}>
          <Error />
        </React.Suspense>
      }
    }

    return (
      <Tooltip title={this.props.language} style={this.props.style}>
        {component}
      </Tooltip>
    )
  }
}