import Typography from "@material-ui/core/Typography";
import * as React from 'react';

export default class UMLIcon extends React.PureComponent<{ size?: number }> {
  render() {
    return (
      <Typography component="div" style={{ color: "black", fontWeight: 'bold' }}>UML</Typography>
    )
  }
}