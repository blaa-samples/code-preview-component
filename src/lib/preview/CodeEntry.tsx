import * as React from 'react';
import { Langs } from '../Languages';

import idea from "react-syntax-highlighter/dist/styles/hljs/idea";

export interface CodeEntryProps {
    language: Langs,
    content: String | undefined
}

export class CodeEntry extends React.PureComponent<CodeEntryProps> {
    render() {
        const SyntaxHighlighter = React.lazy(() => import("react-syntax-highlighter/dist/esm/light-async"));

        return (
            <React.Suspense fallback={<div>Loading syntax highlighting...</div>}>
                <SyntaxHighlighter
                    language={this.props.language.toString().toLowerCase()}
                    style={idea}
                    wrapLines={true}
                    showLineNumbers={true}
                >
                    {this.props.content}
                </SyntaxHighlighter>
            </React.Suspense>
        )
    }
}

export function isCodeEntry(obj: any): obj is CodeEntry {
    return React.isValidElement(obj) && (obj.props as any).language !== "undefined";
}