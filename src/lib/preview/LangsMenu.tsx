import * as React from 'react';
//import { Menu, MenuItem, Typography } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import { Langs } from '../Languages';
import LanguageIcon from '../icons/LanguageIcon';

export interface LangsMenuProps {
    anchor: HTMLElement,
    selected: Langs,
    open: boolean,
    languages: Array<Langs>,

    // Functions
    onClick: (language: Langs) => void
}

const LangsMenu: React.SFC<LangsMenuProps> = (props) => {
    return (
        <Menu
            open={props.open}
            anchorEl={props.anchor}
        >
            {Array.from(props.languages).map((lang) => {
                return (
                    <MenuItem
                        key={lang}
                        onClick={() => props.onClick(lang)}
                        selected={props.selected === lang}
                        disabled={props.selected === lang}
                    >
                        <LanguageIcon language={lang} />
                        <Typography component="div" style={{ marginLeft: 5 }}>{lang}</Typography>
                    </MenuItem>
                )
            })}
        </Menu>
    )
}

export default LangsMenu;