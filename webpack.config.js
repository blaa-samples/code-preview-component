const path = require("path");
const Visualizer = require('webpack-visualizer-plugin');
const CompressionPlugin = require("compression-webpack-plugin");

module.exports = {
    entry: './src/lib/index.ts',
    output: {
        filename: "[name].js",
        chunkFilename: "[name].chunk.js",
        path: path.join(__dirname, "dist"),
        publicPath: path.join(__dirname)
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            // Load styles
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            },

            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            {
                test: /\.tsx?$/,
                include: path.join(__dirname, "src/lib"),
                loader: "awesome-typescript-loader",
                options: {
                    configFileName: "tsconfig-lib.json"
                }
            },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {
                enforce: "pre",
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "source-map-loader"
            },

            // load html files
            { test: /\.html$/, loader: "html-loader" }
        ],
    },

    plugins: [
        new CompressionPlugin({
            test: /\.js(\?.*)?$/i
        }),
        new Visualizer()
    ],

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: [
        {
            "react": "react",
            "react-dom": "react-dom",
            "react-syntax-highlighter/dist/esm/light-async": "react-syntax-highlighter/dist/esm/light-async"
        }
    ]
};